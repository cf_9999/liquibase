package com.example.liquibase;//package com.example.liquibases_test;
//
//import liquibase.integration.spring.SpringLiquibase;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//import javax.sql.DataSource;
//
///**
// * liquibase配置
// */
//@Configuration
//public class LiquibaseConfig {
//
//    @Bean
//    SpringLiquibase liquibase(DataSource dataSource) {
//        SpringLiquibase liquibase = new SpringLiquibase();
//        liquibase.setDataSource(dataSource);
//        liquibase.setChangeLog("classpath:liquibase/master.xml");
//        liquibase.setShouldRun(true);
//        return liquibase;
//    }
//}
