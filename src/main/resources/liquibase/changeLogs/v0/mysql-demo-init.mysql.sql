-- liquibase formatted sql

-- changeset Administrator:1667727526478-1
CREATE TABLE user (id BIGINT AUTO_INCREMENT NOT NULL, name VARCHAR(25) DEFAULT '' NULL COMMENT '姓名', birthday timestamp DEFAULT NOW() NULL COMMENT '生日', age INT DEFAULT 0 NULL COMMENT '年龄', sex BIT DEFAULT 0 NULL COMMENT '性别：0-其他，1-男，2-女', addr VARCHAR(100) NULL COMMENT '地址', phone VARCHAR(20) NULL COMMENT '电话', CONSTRAINT PK_USER PRIMARY KEY (id), UNIQUE (name));

-- changeset Administrator:1667727526478-2
INSERT INTO user (id, name, birthday, age, sex, addr, phone) VALUES (1, '张三', '2022-11-06 17:31:43', 13, 1, 'd1', '');
INSERT INTO user (id, name, birthday, age, sex, addr, phone) VALUES (2, '李四', '2022-11-06 17:31:43', 15, 1, 'd2', '');
INSERT INTO user (id, name, birthday, age, sex, addr, phone) VALUES (3, '王五', '2022-11-06 17:31:43', 18, 1, 'd3', '');
INSERT INTO user (id, name, birthday, age, sex, addr, phone) VALUES (4, '孙二', '2022-11-06 17:31:43', 25, 1, 'd4', '');
INSERT INTO user (id, name, birthday, age, sex, addr, phone) VALUES (5, '马六', '2022-11-06 17:31:43', 38, 1, 'd5', NULL);

-- changeset Administrator:1667727526478-3
CREATE INDEX user_sex_IDX ON user(sex, age);

